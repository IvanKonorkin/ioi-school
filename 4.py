n = int(input())
v = [0] * 9
for i in range(n):
    q = int(input())
    if q < 37:
        q = (q - 1) // 4
    else:
        q = 8 - (q - 37) // 2
    v[q] += 1
m = 0
l = 0
for i in range(9):
    if v[i] == 6:
        l += 1
    m = max(m, l)
print(m)
