m = int(input())
if m < 3 or m == 5:
    print(0, 0)
else:
    print(m // 3 - m % 3, m % 3)
