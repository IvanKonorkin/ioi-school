n = int(input())
k = int(input())
empty = [True] * n + [False]
max_length = 0
for i in range(k):
    start = 0
    max_length = 0
    length = 0
    for j in range(n+1):
        if empty[j]:
            length += 1
        else:
            length = 0
        if length >= max_length:
            max_length = length
            start = j - length + 1
    empty[start + max_length // 2] = False
    # print(empty)
print(max_length // 2 - 1 + max_length % 2, max_length // 2)
